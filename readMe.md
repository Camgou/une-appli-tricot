# Cahier des charges pour le développement d’une application tricot.

Le but est de réaliser une application autour du tricot. 
Je souhaiterais que cette application compte 3 grandes fonctionnalités : 
* Un compteur de rang
* Un espace de stockage
* La possibilité de générer des instructions rang après rang.

## 1. Un compteur de rangs 
L’écran sera constitué d’un bouton **_+1_**. A chaque fin de rang l’utilisateur pourra comptabiliser son rang en appuyant sur ce bouton.
Il pourra garder plusieurs compteurs de rangs en mémoire, les personnes qui tricotent ont souvent plusieurs projets en cours et sur un même projet il y a des fois besoin de plusieurs compteurs.
S’il le souhaite l’utilisateur pourra lui donner un nom, lui donner un nombre total de rangs et le relier à un projet. 
A tout moment il pourra corriger son nombre de rangs (bouton **_-1_**), le remettre à 0 ou changer le total de rangs.
Ce serait bien qu’il puisse personnaliser l’affichage du compteur. (la taille de la police, la couleur, une photo de fond …).

## 2. Un espace dédié où les utilisateurs peuvent : 
* Stocker des modèles, patrons, ou photo de réalisations (par eux mêmes ou d’autres pour inspiration),

=> Dans le futur le but serait que les utilisateurs puissent :
* interagir entre eux, via un forum, 
* consulter les réalisations des uns ou des autres, 
* commenter les réalisations des uns ou des autres, 
* s’échanger des patrons ou modèles.

## 3. Un formulaire permettant de rentrer les instructions dans l’application :
Pas à pas l’utilisateur rentrera les étapes décrites sur le modèle, ainsi il n’aura plus qu’à suivre les instructions à l’écran rang après rang.

### Des instructions rangs après rangs : 
Selon le formulaire rempli ou les instructions pré-enregistrées, le but est qu’à chaque rang terminé on indique à l’utilisateur ce qu’il doit faire au rang suivant (le point, les augmentations/diminutions, la couleur …). 
L’utilisateur pourra ensuite en générer un pdf s’il veut l’imprimer ou pour plus de confort de lecture (si le but n’est pas d’utiliser le compteur de rangs).

Il pourra aussi modifier manuellement les instructions à certaines étapes (revenir en arrière d’un ou plusieurs rangs, ajouter des rangs à l’objectif final …)

Il faudrait que l’utilisateur puisse choisir d’afficher les instructions selon la norme qu’il souhaite (ex : 1m. = 1 maille, col. = coloris etc …). 

A l’écran l’utilisateur sait :
* combien de rang il a fait sur un certain total de rangs à faire
* combien de rang depuis la dernière action
* combien de rang avant la prochaine action (comme un GPS, dans 500m tournez à gauche : dans 6 rangs augmentez de 2 mailles).

Eléments à l’écran :
* Bouton **_+1 -1_**
* Switch to PDF view
* Retour autres projets


### Prise en charge des fichiers pdf : 
Si l’utilisateur ne souhaite pas saisir les différentes étapes de son projet, il pourra se contenter de lier un fichier pdf à son projet. Il faudrait qu’il puisse annoter le pdf : 
Exemple 1 : pour les patrons qui donnent plusieurs tailles il faudrait qu’il puisse souligner la taille qu’il souhaite sur son pdf et ainsi minimiser les risques de se tromper de taille au cours de sa réalisation.
Exemple 2 : Qu’il puisse faire une marque comme un gros point pour marquer là où il s’est arrêté et faciliter la reprise de son travail par la suite.
Exemple 3 : S’il souhaite modifier un nombre de mailles par ex, qu’il puisse noter dans la marge le nombre de maille qu’il souhaite, comme quand on annote un livre de recettes.


### Une bibliothèque de patrons rentrés dans l’application :
Ce sera des patrons qu’on trouve gratuitement sur internet que j’aurai manuellement rentrés.
A terme le but serait que cette bibliothèque soit alimentée par les membres de la communauté. En gardant à l’esprit qu’il faut les patrons partagés soient disponibles gratuitement ailleurs.

L’affichage se fera sous forme de petites vignettes à la manière de Podcast Addict.

![Capture d’écran](Screenshot_20190325-155943.png "Capture d’écran de Podcast Addict")

On pourra mettre différents filtres pour afficher les vignettes et ainsi choisir l’ordre dans lequel on les affiche. (les plus consultées, les plus tricotées, les plus récentes, par genre [bonnets, pulls, layettes …].


On peut également envisager une option de e-commerce où les enseignes ou les créateur peuvent vendre leurs patrons.
Est-ce que la vente se fait sur le site de l’enseigne et en même temps que le pdf l’utilisateur récupère un code pour importer le patron dans son application ou est-ce que la vente se fait directement dans l’application ? Voire les 2 ?


### Divers :

Penser écrire suffisamment gros pour ma mère qui tricote avec des lunettes.
Rendre l’application la plus intuitive possible pour ma mère qui n’a pas les mêmes réflexes que moi en terme d’application. De ce fait, je pense que lors de sa première utilisation l’usager doit arriver sur un compteur de rangs très simple à prendre en main. S’il souhaite naviguer et explorer le reste des fonctionnalités libre à lui de le faire.

### Transformation de photos ou dessin en patron.
En rentrant une photo ou un dessin dans l’application, l’utilisateur peut générer le motif sous forme de patron.
L’utilisateur rentre la taille du motif en cm (ou mm ?), l’épaisseur de la laine et la taille de l’aiguille et l’outil génère une grille indiquant le nombre de maille et la couleur utilisée. Il faudra une fonction aperçu avant de générer la photo.
Dans un premier temps on peut imaginer 4 couleurs maximum et 1 seul point : le jersey.

### RGPD

Le site ne permet pas de vérifier de manière automatique si le patron proposé existe déjà et si le droit d’auteur est respecté. Charge au modérateur de faire respecter la loi.
